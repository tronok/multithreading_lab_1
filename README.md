# Report

Lab1 Multithreading programming, Kalashnikov Dmitry, M4105.

## PC

* Memory: 12GB
* CPU: Inter Core i5-4200 CPU @ 1600GHz
* SSD
* OS:64bits

## Exploratory plots
![Static scheduler](https://bytebucket.org/tronok/multithreading_lab_1/raw/bf40244e60a25d83b709b7eb2981f956a9626e14/sch.1.png)
![Dynamic scheduler](https://bytebucket.org/tronok/multithreading_lab_1/raw/bf40244e60a25d83b709b7eb2981f956a9626e14/sch.2.png)
![Guided scheduler](https://bytebucket.org/tronok/multithreading_lab_1/raw/bf40244e60a25d83b709b7eb2981f956a9626e14/sch.3.png)

## Conclusion

* In most cases efficiency is higher for the 2 threads probably because other two cores are busy with other tasks
* Guided scheduler greatly eliminates difference in efficiency among small and big matrix. 
However it declines typically efficiency as that can be noticed from compare with static scheduler 