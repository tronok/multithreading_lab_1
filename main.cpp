#include <iostream>
#include <fstream>
#include <chrono>
#include "Matrix.h"
#include <vector>

Matrix readMatrix(char name[]) {
    std::ifstream input(name);
    char c = 0;
    int colCounter = 0;
    while (input.get(c) && c != '\n') {
        if (c == ' ') {
            ++colCounter;
        }
    }
    if (colCounter == 0)
    {
        throw std::invalid_argument("Input file is empty one");
    }
    ++colCounter;
    int rowCounter = 1;

    bool hasAnyOtherSymb = false;
    bool lastSymbolNewLine = false;
    while (input.get(c)) {
        hasAnyOtherSymb = true;
        if (c == '\n') {
            ++rowCounter;
            lastSymbolNewLine = true;
        }
        else
        {
            lastSymbolNewLine = false;
        }
    }
    if (hasAnyOtherSymb) {
        ++rowCounter;
    }
    if (lastSymbolNewLine)
    {
        --rowCounter;
    }
    std::ifstream input2(name);
    std::vector<std::vector<int>> matrix(rowCounter);
    for (int i = 0; i != rowCounter; ++i) {
        matrix[i] = std::vector<int>(colCounter);
        for (int j = 0; j != colCounter; ++j) {
            input2 >> matrix[i][j];
            //std::cout << matrix[i][j] << ' ';
        }
        //std::cout << std::endl;
    }
    return Matrix(std::move(matrix));
}

void writeMatrix(const Matrix &matrix, char path[]) {
    std::ofstream output(path);
    for (int i = 0; i != matrix.rowNum; ++i) {
        for (int j = 0; j != matrix.colNum - 1; ++j) {
            output << matrix.mat[i][j] << " ";
        }
        output << matrix.mat[i][matrix.colNum - 1] << std::endl;
    }
}

int main(int argc, char *argv[]) {
    if (argc != 4) {
        std::cerr <<
        "The first and the second arguments should be paths to first and second matrixes. The third argument is path to result file";
        return 1;
    }

    try
    {
        Matrix firstMatrix = readMatrix(argv[1]);
        Matrix secondMatrix = readMatrix(argv[2]);
        if (firstMatrix.colNum != secondMatrix.rowNum) {
            throw std::invalid_argument("Matrix multiplication cannot be done inasmuch as dimensions of matrixs are inappropritate");
        }
        int rows = firstMatrix.rowNum;
        int cols = secondMatrix.colNum;
        int multInd = firstMatrix.colNum;
        std::vector<std::vector<int>> result(rows);
        for (int i = 0; i != rows; ++i) {
            result[i] = std::vector<int>(cols);
        }
        const std::vector<std::vector<int>>& fm = firstMatrix.mat;
        const std::vector<std::vector<int>>& sm = secondMatrix.mat;
        int ij;

        int gen = rows * cols;
        std::chrono::high_resolution_clock::time_point begin = std::chrono::high_resolution_clock::now();
#pragma omp parallel for shared(result, fm, sm) firstprivate(rows, cols, multInd, gen)
        for (ij = 0; ij < gen; ++ij) {
            int j = ij / rows;
            int i = ij % cols;
            result[i][j] = 0;
            for (int k = 0; k != multInd; ++k) {
                result[i][j] += fm[i][k] * sm[k][j];
            }
        }
        long long res = std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::high_resolution_clock::now() - begin).count();
        std::cout << "Computation time " << res << std::endl;
        Matrix resMatrix(std::move(result));
        writeMatrix(resMatrix, argv[3]);
        return 0;
    }
    catch (std::exception& e)
    {
        std::ofstream output(argv[3]);
        output << e.what() << std::endl;
        return 1;
    }

}