//
// Created by jetbrains on 21.10.2015.
//


#include <stdlib.h>
#include <iosfwd>
#include <fstream>
#include "Matrix.h"
#include <omp.h>
#include <chrono>
#include <iostream>

Matrix generateMatrix(int rows, int cols)
{
    std::vector<std::vector<int>> result(rows);
    for (int i = 0; i != rows; ++i) {
        result[i] = std::vector<int>(cols);
        for (int j = 0; j != cols; ++j)
        {
            result[i][j] = rand();
        }
    }
    return Matrix(result);
}



int main(int argc, char *argv[])
{
    std::ofstream metrics("metrics.csv");
    int iterNum = 100;
    metrics << "scheduler,size,threanum,";
    for (int iter = 0; iter != iterNum - 1; ++iter)
    {
     metrics << "inv" << iter << ",";
    }
    metrics << "inv" << iterNum << std::endl;
    omp_sched_t schedulers[] = {omp_sched_static, omp_sched_dynamic, omp_sched_guided};
    int matrixSizes[] = { 50, 100, 200, 400 };
    int threadNums[] = {1, 2, 3, 4, 5, 6};
    for (omp_sched_t scheduler: schedulers)
    {
        for (int size: matrixSizes)
        {
            for(int threadNum: threadNums)
            {
                std::cout << scheduler << " " << size << " " << threadNum << std::endl;
                Matrix first = generateMatrix(size, size);
                Matrix second = generateMatrix(size, size);
                std::vector<std::vector<int>> fm = first.mat;
                std::vector<std::vector<int>> sm = second.mat;
                omp_set_schedule(scheduler, 0);
                omp_set_num_threads(threadNum);
                int ij;
                int gen = size * size;
                int **result = new int *[size];
                for (int i = 0; i != size; ++i) {
                    result[i] = new int[size];
                }
                metrics << scheduler << "," << size << "," << threadNum << ",";
                for (int m = 0; m != iterNum; ++m)
                {
                    std::chrono::high_resolution_clock::time_point begin = std::chrono::high_resolution_clock::now();
#pragma omp parallel for shared(result, fm, sm) firstprivate(size,  gen)
                    for (ij = 0; ij < gen; ++ij) {
                        int j = ij / size;
                        int i = ij % size;
                        result[i][j] = 0;
                        for (int k = 0; k != size; ++k) {
                            result[i][j] += fm[i][k] * sm[k][j];
                        }
                    }
                        long long res = std::chrono::duration_cast<std::chrono::microseconds>(
                            std::chrono::high_resolution_clock::now() - begin).count();
                    metrics << res;
                        if (m != iterNum - 1)
                    {
                        metrics << ",";
                    }
                    else
                    {
                        metrics << std::endl;
                    }
                }

            }
        }
    }
    metrics.close();
    return 0;
}