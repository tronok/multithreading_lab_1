//
// Created by jetbrains on 21.10.2015.
//

#ifndef LAB1_MATRIX_H
#define LAB1_MATRIX_H

#include <vector>

struct Matrix {
    int rowNum, colNum;
    std::vector<std::vector<int>> mat;

    Matrix(std::vector<std::vector<int>>&& mt) {
        mat = std::move(mt);
        int prev = -1;
        for (int i = 0; i != mat.size(); ++i)
        {
            if (prev == -1){
                prev = mat[i].size();
            }
            else  if (mat[i].size() != prev)
            {
               throw std::invalid_argument("Length of rows must be the same");
            }
        }
        rowNum = mat.size();
        colNum = prev;
    }

};

#endif //LAB1_MATRIX_H
